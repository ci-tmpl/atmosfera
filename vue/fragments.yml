# This is reusable fragments for CI configuration which should be used to reduce code duplication
# Build and deploy frontend e2e test or review apps --------------------------------------------------------------------
#
# Variables:
# * APP_FE_UNBUILD_PATH
# * APP_FE_RAW_PATH
# * APP_FE_PATH
#
# Inherited Variables:
# * APP_FE_URL
# * APP_FE_API_URL
# * IS_NUXT
#
# Inherited SaaS variables:
# * APP_FE_E2E_INSTANCE_SLUG
# * APP_FE_API_INSTANCE_URL
# * APP_FE_API_MAIN_URL
#
.e2e/ra-frontend-deploy:
  script:
    # ---
    - echo $APP_FE_UNBUILD_PATH
    - echo $APP_FE_RAW_PATH
    - echo $APP_FE_PATH
    # ---
    - if [ -d "$APP_FE_UNBUILD_PATH" ]; then rm -Rf $APP_FE_UNBUILD_PATH; fi  # remove temp e2e/ra folder if exist
    - cp -rd $APP_FE_RAW_PATH $APP_FE_UNBUILD_PATH                            # copy inbuilt project files to temp e2e/ra folder (-d = with symbolic links)
    - cd $APP_FE_UNBUILD_PATH                                                 # enter to temp e2e/ra folder
    # build
    - !reference [ .frontend-build, script ]                                  # import frontend build commands from a fragment
    # deploy artefacts
    - if [ -d "$APP_FE_PATH" ]; then rm -Rf $APP_FE_PATH; fi                  # remove temp e2e/ra folder if exist
    - cp -rd $APP_FE_UNBUILD_PATH/dist $APP_FE_PATH                           # copy built project files to temp e2e/ra folder (-d = with symbolic links)

# Build frontend commands ----------------------------------------------------------------------------------------------
#
# Variables:
# * APP_FE_URL
# * APP_FE_API_URL
# * APP_FE_E2E_INSTANCE_SLUG
# * IS_NUXT
#
# SaaS variables:
# * APP_FE_API_INSTANCE_URL
# * APP_FE_API_MAIN_URL
#
.frontend-build:
  script:
    # ---
    - echo $APP_FE_URL
    - echo $APP_FE_API_URL
    - echo $APP_FE_E2E_INSTANCE_SLUG
    - echo $APP_FE_API_INSTANCE_URL
    - echo $APP_FE_API_MAIN_URL
    - echo $IS_NUXT
    # ---
    # install dependencies and link bit.cloud components
    - yarn install
    # configure frontend .env file for e2e
    - cp .env.example .env
    - sed -i "s|{{VUE_APP_DOMAIN}}|$APP_FE_URL|g" .env                            # set VUE_APP_DOMAIN
    - sed -i "s|{{VUE_APP_API_DOMAIN}}|$APP_FE_API_URL|g" .env                    # set VUE_APP_API_DOMAIN
    # SaaS related variables only! (skips if not exist)
    - sed -i "s|{{VUE_APP_INSTANCE_API_DOMAIN}}|$APP_FE_API_INSTANCE_URL|g" .env  # set VUE_APP_API_DOMAIN
    - sed -i "s|{{VUE_APP_MAIN_API_DOMAIN}}|$APP_FE_API_MAIN_URL|g" .env          # set VUE_APP_MAIN_API_DOMAIN
    - sed -i "s|{{VUE_APP_E2E_APP_NAME}}|$APP_FE_E2E_INSTANCE_SLUG|g" .env        # set VUE_APP_E2E_APP_NAME
    # tracking script variables
    - sed -i "s|{{VUE_APP_GTM_ID}}|$VUE_APP_GTM_ID|g" .env                        # set VUE_APP_GTM_ID (Google tag manager id)
    - sed -i "s|{{VUE_APP_META_ID}}|$VUE_APP_META_ID|g" .env                      # set VUE_APP_META_ID (Meta pixel id)
    # set project related environment variables (LOCAL fragment)
    - !reference [ .frontend-custom-env-set, script ]
    # build frontend
    - if [[ $IS_NUXT ]]; then yarn generate ; else yarn build ; fi

# Prepare slugs (to fits max allowed subdomain length = 63 characters) -------------------------------------------------
#
.ra-frontend-slugs:
  script:
    - FE_RA_SLUG=$(echo ra-$SUB_DOMAIN_DEV-$CI_COMMIT_REF_SLUG | cut -c -63)
    - FE_RA_UNBUILD_SLUG=$(echo ra-$SUB_DOMAIN_DEV-unbuild-$CI_COMMIT_REF_SLUG | cut -c -63)

.ra-frontend-api-slugs:
  script:
    - API_RA_SLUG=$(echo ra-$API_SUB_DOMAIN_DEV-$RA_API_COMMIT_REF_SLUG | cut -c -63)            # use variable from ServiceApp = $RA_API_COMMIT_REF_SLUG
    - API_RA_MAIN_SLUG=$(echo ra-$API_MAIN_SUB_DOMAIN_DEV-$RA_API_COMMIT_REF_SLUG | cut -c -63)  # use variable from ServiceApp = $RA_API_COMMIT_REF_SLUG

#
# Local fragments (copy it from here to projects .gitlab-ci.yml file)
# Sets environment variables by replacing template strings "{{...}}" in .env file --------------------------------------
#
.frontend-custom-env-set:
  script:
    - > # Set variables only for DEVELOPMENT environment
      if [[ $CI_ENVIRONMENT_SLUG == 'development' ]]; then
        sed -i "s|{{VARIABLE}}|$VARIABLE|g" .env;
      fi
    - > # Set variables only for PRODUCTION environment
      if [[ $CI_ENVIRONMENT_SLUG == 'production' ]]; then
        sed -i "s|{{VARIABLE}}|$VARIABLE|g" .env;
      fi
    # Set variables for ALL environments or for environment related CI/CD variables
    - sed -i "s|{{VARIABLE}}|$VARIABLE|g" .env
